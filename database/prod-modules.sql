-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: yody-production.cpl2fenruiiq.ap-southeast-1.rds.amazonaws.com:3306
-- Thời gian đã tạo: Th1 22, 2022 lúc 04:49 AM
-- Phiên bản máy phục vụ: 8.0.23
-- Phiên bản PHP: 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `prod_auth_service`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `modules`
--

CREATE TABLE `modules` (
  `id` bigint NOT NULL,
  `code` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  `updated_by` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `version` smallint DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `check_store` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `modules`
--

INSERT INTO `modules` (`id`, `code`, `created_by`, `created_date`, `created_name`, `is_deleted`, `updated_by`, `updated_date`, `updated_name`, `version`, `name`, `status`, `check_store`) VALUES
(1, 'admin', 'admin', '2021-06-12 10:07:05', 'admin', 0, 'admin', '2021-06-12 10:12:05', 'admin', 1, 'Quản trị', 'ACTIVE', 0),
(2, 'customers', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'customers', 'ACTIVE', 0),
(3, 'ecommerces', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'ecommerces', 'ACTIVE', 0),
(4, 'loyalties', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'loyalties', 'ACTIVE', 0),
(5, 'products', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'Sản phẩm', 'ACTIVE', 0),
(7, 'orders', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'order', 'active', 0),
(8, 'stores', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'Cài đặt', 'ACTIVE', 0),
(9, 'channels', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'Kênh bán hàng', 'ACTIVE', 0),
(10, 'sources', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'Nguồn đơn', 'ACTIVE', 0),
(11, 'prints_templates', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'Mẫu in', 'ACTIVE', 0),
(12, 'accounts', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'Tài khoản', 'ACTIVE', 0),
(13, 'purchase_orders', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'Nhập hàng', 'ACTIVE', 0),
(14, 'suppliers', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'Nhà cung cấp', 'ACTIVE', 0),
(15, 'inventory_transfer', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'Chuyển kho', 'ACTIVE', 0),
(16, 'inventory_adjustment', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'Kiểm kho', 'ACTIVE', 0),
(17, 'logistic_gateways', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'Vận chuyển', 'ACTIVE', 0),
(18, 'price_rules', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'Khuyến mại', 'ACTIVE', 0),
(19, 'auths', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'Nhóm quyền', 'ACTIVE', 0),
(20, 'departments', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'Phòng ban', 'ACTIVE', 0);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `modules`
--
ALTER TABLE `modules`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
