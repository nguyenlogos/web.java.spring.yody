

INSERT INTO `roles` (`id`, `code`, `created_by`, `created_date`, `created_name`, `is_deleted`, `updated_by`, `updated_date`, `updated_name`, `version`, `name`, `status`, `description`) VALUES
(1, 'admin', 'admin', '2021-06-12 10:07:05', 'admin', 0, 'yd0579', '2022-01-21 10:39:45', 'Trần Phạm Nguyên Khang', 17, 'Quản trị viên', 'ACTIVE', NULL),
(2, '141eabb7-fe47-43a8-ac2f-b8f536064221', 'YD9999', '2021-11-12 21:37:11', 'Admin', 0, 'YD99999', '2021-12-28 16:43:38', 'admin', 9, 'Nhân viên bán hàng', 'ACTIVE', 'Chuyên gia tư vấn'),
(3, 'a67fbfc3-230e-4011-b4e0-9ce12f91b6e7', 'YD9999', '2021-11-15 09:44:44', 'Admin', 0, 'YD9999', '2021-11-15 09:44:44', 'Admin', 1, 'Merchandiser', 'ACTIVE', 'Nhân viên quản lý đơn hàng'),
(4, '4b6110bb-dbde-4aed-a979-1b3fe431f629', 'YD9999', '2021-11-16 22:07:28', 'Admin', 0, 'YD9999', '2021-11-16 22:07:28', 'Admin', 1, 'Cửa hàng trưởng', 'ACTIVE', NULL),
(5, '7ad9dc25-d7be-45a9-a030-15199d8dedae', 'YD9999', '2021-11-16 23:56:46', 'Admin', 0, 'YD9999', '2021-11-16 23:56:46', 'Admin', 1, 'Thu ngân', 'ACTIVE', NULL),
(6, 'c2e639e3-ade5-449b-89ea-3a34640c7e28', 'YD9999', '2021-11-18 11:30:58', 'Admin', 0, 'YD9999', '2021-11-18 11:30:58', 'Admin', 1, 'Xem hệ thống', 'ACTIVE', 'Default account'),
(7, '4f7d65fd-de45-4088-b10e-031993660f05', 'YD9999', '2021-11-18 16:03:46', 'Admin', 0, 'YD9999', '2021-11-18 16:03:46', 'Admin', 1, 'CV Ngành hàng', 'ACTIVE', NULL),
(8, '636e0d02-6d72-4adf-aa33-a33c61d40a9a', 'YD9999', '2021-11-22 10:24:41', 'Admin', 0, 'YD9999', '2021-11-27 16:20:27', 'Admin', 9, 'KIM', 'ACTIVE', 'sắt'),
(9, '4feca612-ce1e-43d7-96ab-d630b13e3fc0', 'YD9999', '2021-11-27 14:59:33', 'Admin', 0, 'YD9999', '2021-11-27 14:59:33', 'Admin', 1, 'ASM', 'ACTIVE', 'Quản lý vùng, xem báo cáo'),
(10, '14a49c06-c3b9-41dd-80bb-c5d1d58f283d', 'YD9999', '2021-12-01 16:42:56', 'Admin', 0, 'YD9999', '2021-12-01 16:42:56', 'Admin', 1, 'Kiểm soát viên', 'ACTIVE', NULL),
(11, '5cc8fe00-4985-4676-b62f-40fcd8746cca', 'YD9999', '2021-12-01 16:45:44', 'Admin', 0, 'YD9999', '2021-12-01 16:45:44', 'Admin', 1, 'Đối soát viên', 'ACTIVE', NULL),
(12, '727ca3c9-83d4-44f2-8b5b-ed4234fb3718', 'YD9999', '2021-12-01 16:46:57', 'Admin', 0, 'YD9999', '2021-12-01 16:46:57', 'Admin', 1, 'Kế toán', 'ACTIVE', NULL),
(13, 'b7276e00-faa3-4468-9e12-a60063e03361', 'YD9999', '2021-12-01 16:50:41', 'Admin', 0, 'YD9999', '2021-12-01 16:50:41', 'Admin', 1, 'Nhân viên kho', 'ACTIVE', NULL),
(14, '0a26374c-3451-43b4-a884-accc08559875', 'YD9999', '2021-12-01 16:52:27', 'Admin', 0, 'YD9999', '2021-12-01 16:52:27', 'Admin', 1, 'CSKH', 'ACTIVE', NULL),
(15, '90ca6ece-7113-4099-919f-5518560f6623', 'YD9999', '2021-12-01 16:53:27', 'Admin', 0, 'YD66883', '2022-01-04 11:47:22', 'Nguyễn Liên', 29, 'Giám đốc', 'ACTIVE', NULL),
(16, '415492d8-05d4-4b3b-9e46-b6ef8f430d8e', 'YD9999', '2021-12-01 16:54:47', 'Admin', 0, 'YD9999', '2021-12-01 16:54:47', 'Admin', 1, 'Thủ kho', 'ACTIVE', NULL),
(17, 'b7a87e98-f748-466b-95a1-7430dff9d6f6', 'YD9999', '2021-12-01 16:56:43', 'Admin', 0, 'YD9999', '2021-12-01 16:56:43', 'Admin', 1, 'Nhân viên thu mua', 'ACTIVE', NULL),
(18, 'e10df41f-e2f0-4095-a033-2e7b70ca3622', 'YD99999', '2021-12-20 17:36:44', 'Admin', 0, 'YD99999', '2022-01-06 10:35:02', 'admin', 9, 'Nhân viên YODY', 'ACTIVE', NULL);

