-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: yody-production.cpl2fenruiiq.ap-southeast-1.rds.amazonaws.com:3306
-- Thời gian đã tạo: Th1 22, 2022 lúc 04:43 AM
-- Phiên bản máy phục vụ: 8.0.23
-- Phiên bản PHP: 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `prod_auth_service`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint NOT NULL,
  `code` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `created_by` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  `updated_by` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `version` smallint DEFAULT NULL,
  `module_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `permissions`
--

INSERT INTO `permissions` (`id`, `code`, `created_by`, `created_date`, `created_name`, `is_deleted`, `updated_by`, `updated_date`, `updated_name`, `version`, `module_code`, `name`) VALUES
(1, 'all', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'admin', 'Administrator permission'),
(32, 'read', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'customers', 'Danh sách khách hàng'),
(33, 'create', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'customers', 'Tạo khách hàng'),
(34, 'update', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'customers', 'Sửa khách hàng'),
(36, 'export', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'customers', 'Xuất excel'),
(37, 'groups_read', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'customers', 'Xem nhóm khách hàng'),
(38, 'groups_create', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'customers', 'Tạo nhóm khách hàng'),
(39, 'groups_update', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'customers', 'Sửa nhóm khách hàng'),
(40, 'groups_delete', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'customers', 'Xoá nhóm khách hàng'),
(41, 'levels_read', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'customers', 'Xem hạng khách hàng'),
(42, 'levels_create', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'customers', 'Tạo hạng khách hàng'),
(43, 'levels_update', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'customers', 'Sửa hạng khách hàng'),
(44, 'levels_delete', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'customers', 'Xoá hạng khách hàng'),
(45, 'types_read', 'admin', '2021-06-12 10:11:07', 'admin', 1, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'customers', 'view type'),
(46, 'types_create', 'admin', '2021-06-12 10:11:07', 'admin', 1, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'customers', 'create type'),
(47, 'types_update', 'admin', '2021-06-12 10:11:07', 'admin', 1, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'customers', 'update type'),
(48, 'types_delete', 'admin', '2021-06-12 10:11:07', 'admin', 1, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'customers', 'delete type'),
(49, 'shops_read', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'ecommerces', 'Xem danh sách gian hàng'),
(51, 'shops_connect', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'ecommerces', 'Kết nối gian hàng'),
(52, 'shops_delete', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'ecommerces', 'Xoá gian hàng'),
(53, 'shops_update', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'ecommerces', 'Sửa gian hàng'),
(54, 'variants_read', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'ecommerces', 'Xem danh sách sản phẩm'),
(55, 'variants_update', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'ecommerces', 'Ghép nối sản phẩm'),
(56, 'variants_delete', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'ecommerces', 'Xoá sản phẩm'),
(57, 'variants_update_stock', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'ecommerces', 'Đồng bộ tồn kho'),
(58, 'variants_download', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'ecommerces', 'Tải sản phẩm từ sàn về'),
(59, 'variants_disconnect', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'ecommerces', 'Ngắt kết nối sản phẩm với sàn'),
(60, 'orders_download', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'ecommerces', 'Tải đơn hàng'),
(61, 'cards_assignment', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'loyalties', 'Gán thẻ'),
(62, 'cards_lock', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'loyalties', 'Khoá thẻ'),
(63, 'cards_read', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'loyalties', 'Xem danh sách thẻ'),
(64, 'cards_release', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'loyalties', 'Tạo đợt phát hành thẻ'),
(65, 'cards_read', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'loyalties', 'Xem danh sách đợt phát hành thẻ'),
(66, 'programs_read', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'loyalties', 'Xem sanh sách chương trình'),
(67, 'programs_create', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'loyalties', 'Thêm chương trình'),
(68, 'programs_update', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'loyalties', 'Sửa chương trình'),
(70, 'programs_delete', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'loyalties', 'Xoá chương trình'),
(71, 'config', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'loyalties', 'Cấu hình'),
(72, 'points_update', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'loyalties', 'Tạo mới phiếu thay đổi'),
(73, 'categories_create', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'products', 'Tạo danh mục'),
(74, 'categories_update', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'products', 'Sửa danh mục'),
(75, 'categories_read', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'products', 'Xem danh mục'),
(76, 'categories_delete', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'products', 'Xóa danh mục'),
(77, 'colors_create', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'products', 'Tạo màu sắc'),
(78, 'colors_update', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'products', 'Sửa màu sắc'),
(79, 'colors_read', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'products', 'Xem màu sắc'),
(80, 'colors_delete', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'products', 'Xóa màu sắc'),
(81, 'materials_create', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'products', 'Tạo chất liệu'),
(82, 'materials_update', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'products', 'Sửa chất liệu'),
(83, 'materials_read', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'products', 'Xem chất liệu'),
(84, 'materials_delete', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'products', 'Xóa chất liệu'),
(85, 'sizes_create', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'products', 'Tạo kích thước'),
(86, 'sizes_update', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'products', 'Sửa kích thước'),
(87, 'sizes_read', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'products', 'Xem kích thước'),
(88, 'sizes_delete', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'products', 'Xóa kích thước'),
(89, 'read_cost', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'products', 'Xem giá vốn'),
(90, 'update_cost', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'products', 'Sửa giá vốn'),
(91, 'collections_create', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'products', 'Tạo nhóm hàng'),
(92, 'collections_read', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'products', 'Xem nhóm hàng'),
(93, 'collections_update', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'products', 'Sửa nhóm hàng'),
(94, 'collections_delete', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'products', 'Xóa nhóm hàng'),
(95, 'create', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'products', 'Tạo sạn phẩm'),
(96, 'read', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'products', 'Xem sản phẩm cha'),
(97, 'update', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'products', 'Sửa sản phẩm'),
(98, 'delete', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'products', 'Xóa sản phẩm cha'),
(99, 'delete_variant', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'products', 'Xóa sản phẩm'),
(100, 'read_histories', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'products', 'Xem lịch sử sản phẩm'),
(101, 'print_temp', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'products', 'In tem phiếu'),
(102, 'import_excel', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'products', 'Import excel sản phẩm'),
(103, 'upload_image', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'products', 'Upload ảnh sản phẩm'),
(104, 'read_variant', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'products', 'Xem sản phẩm'),
(141, 'create', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'stores', 'Thêm cửa hàng'),
(142, 'read', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'stores', 'Xem cửa hàng'),
(143, 'update', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'stores', 'Cập nhật cửa hàng'),
(144, 'create', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'channels', 'Thêm kênh bán'),
(145, 'update', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'channels', 'Cập nhật kênh bán'),
(146, 'read', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'channels', 'Xem kênh bán'),
(147, 'delete', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'channels', 'Xóa kênh bán'),
(148, 'create', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'sources', 'Thêm nguồn đơn'),
(149, 'update', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'sources', 'Cập nhật nguồn đơn'),
(150, 'read', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'sources', 'Xem nguồn đơn'),
(151, 'delete', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'sources', 'Xóa nguồn đơn'),
(152, 'create', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'prints_templates', 'Thêm mẫu in'),
(153, 'read', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'prints_templates', 'Xem mẫu in'),
(154, 'create', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'accounts', 'Tạo tài khoản'),
(155, 'read', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'accounts', 'Xem tài khoản'),
(156, 'update', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'accounts', 'Cạp nhật tài khoản'),
(157, 'delete', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'accounts', 'Xóa tài khoản'),
(158, 'departments_read', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'accounts', 'Xem phòng ban'),
(159, 'departments_create', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'accounts', 'Tạo phòng ban'),
(160, 'departments_update', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'accounts', 'Cập nhật phòng ban'),
(161, 'departments_delete', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'accounts', 'Xóa phòng ban'),
(162, 'create', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-07-13 10:11:07', 'admin', 1, 'purchase_orders', 'Tạo đơn đặt hàng'),
(163, 'update', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-07-13 10:11:07', 'admin', 1, 'purchase_orders', 'Cập nhật đơn đặt hàng'),
(164, 'delete', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-07-13 10:11:07', 'admin', 1, 'purchase_orders', 'Xóa đơn đặt hàng'),
(165, 'read', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-07-13 10:11:07', 'admin', 1, 'purchase_orders', 'Xóa đon đặt hàng'),
(166, 'cancel', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-07-13 10:11:07', 'admin', 1, 'purchase_orders', 'Hủy đơn đặt hàng'),
(167, 'print', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-07-13 10:11:07', 'admin', 1, 'purchase_orders', 'In đơn đặt hàng'),
(168, 'return', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-07-13 10:11:07', 'admin', 1, 'purchase_orders', 'Tạo trả hàng'),
(169, 'procurements_create', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'purchase_orders', 'Tạo phiếu nhập kho'),
(171, 'procurements_finish', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'purchase_orders', 'Kết thúc nhập kho'),
(172, 'procurements_delete', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'purchase_orders', 'Xóa phiếu nhập kho'),
(173, 'procurements_read', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'purchase_orders', 'Xem phiếu nhập kho'),
(174, 'payments_create', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'purchase_orders', 'Tạo thanh toán'),
(175, 'payments_update', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'purchase_orders', 'Cập nhật thanh toán'),
(176, 'payments_finish', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'purchase_orders', 'Kết thúc thanh toán'),
(177, 'payments_delete', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'purchase_orders', 'Xóa thanh toán'),
(178, 'payments_condition_create', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'purchase_orders', 'Tạo điều khoản thanh toán'),
(179, 'payments_condition_update', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'purchase_orders', 'Cập nhật điều khoản thanh toán'),
(180, 'payments_condition_read', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'purchase_orders', 'Xem điều khoản thanh toán'),
(181, 'payments_condition_delete', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'purchase_orders', 'Xóa điều khoản thanh toán'),
(182, 'suppliers_create', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'suppliers', 'Tạo nhà cung cấp'),
(183, 'suppliers_update', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'suppliers', 'Sửa nhà cung cấp'),
(184, 'suppliers_read', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'suppliers', 'Xem nhà cung cấp'),
(185, 'suppliers_delete', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'suppliers', 'Xóa nhà cung cấp'),
(191, 'approve', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'purchase_orders', 'Duyệt đơn đặt hàng'),
(192, 'payments_approve', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'purchase_orders', 'Duyệt thanh toán'),
(193, 'procurements_confirm', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'purchase_orders', 'Xác nhận nhập'),
(194, 'procurements_approve', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'purchase_orders', 'Duyệt phiếu nháp'),
(195, 'create', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'inventory_transfer', 'Tạo phiếu chuyển kho'),
(196, 'update', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'inventory_transfer', 'Sửa phiếu chuyển kho'),
(197, 'cancel', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'inventory_transfer', 'Hủy phiếu chuyển kho'),
(198, 'read', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'inventory_transfer', 'Xem phiếu chuyển kho'),
(199, 'balance', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'inventory_transfer', 'Cân bằng phiếu chuyển kho'),
(200, 'clone', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'inventory_transfer', 'Tạo bản sao phiếu chuyển kho'),
(201, 'print', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'inventory_transfer', 'In vận đơn/phiếu'),
(202, 'receive', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'inventory_transfer', 'Nhận hàng online'),
(203, 'import', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'inventory_transfer', 'Tạo phiếu chuyển từ excel'),
(204, 'shipment_create', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'inventory_transfer', 'Chuyển hàng'),
(205, 'shipment_delete', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'inventory_transfer', 'Hủy giao hàng'),
(206, 'shipment_export', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'inventory_transfer', 'Xuất kho'),
(207, 'create', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'inventory_adjustment', 'Tạo phiếu kiểm kho'),
(208, 'update', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'inventory_adjustment', 'Sửa phiếu kiểm kho'),
(209, 'audit', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'inventory_adjustment', 'Kiểm kho online'),
(210, 'read', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'inventory_adjustment', 'Xem phiếu kiểm kho'),
(211, 'adjust', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'inventory_adjustment', 'Cân bằng tồn kho'),
(212, 'print', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'inventory_adjustment', 'In biên bản xử lí hàng thừa thiếu'),
(213, 'import', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'inventory_adjustment', 'Import excel(Kiểm kho offline)'),
(214, 'export', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'inventory_adjustment', 'Xuất excel danh sách sản phẩm'),
(232, 'pos_read', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'orders', 'Truy cập vào pos bán hàng (màn hình giao diện pos)'),
(233, 'receipts_create', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'orders', 'Tạo phiếu thu/ Phiếu chi trên POS'),
(234, 'revenue_statistics_read', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'orders', 'Xem kiếm kê tiền trên POS'),
(235, 'daily_reports_read', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'orders', 'Xem báo cáo ngày trên POS'),
(236, 'returns_create', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'orders', 'Đổi trả hàng'),
(237, 'confirmed_update', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'orders', 'Sửa đơn hàng đã xác nhận'),
(238, 'packed_update', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'orders', 'Sửa đơn hàng đóng gói'),
(239, 'shipping_update', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'orders', 'Sửa đơn hàng xuất kho'),
(240, 'finished_update', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'orders', 'Sửa đơn hàng hoàn thành'),
(241, 'confirmed_cancel', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'orders', 'Hủy đơn hàng xác nhận'),
(242, 'packed_cancel', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'orders', 'Hủy đơn hàng đóng gói'),
(243, 'create', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'orders', 'Tạo đơn hàng'),
(244, 'goods_receipt_create', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'orders', 'Tạo biên bản bàn giao'),
(245, 'goods_receipt_read', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'orders', 'Xem biên bản bàn giao'),
(246, 'goods_receipt_delete', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'orders', 'Xóa biên bản bàn giao'),
(247, 'packed_read', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'orders', 'Xem đóng gói'),
(248, 'read', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'orders', 'Xem danh sách đơn hàng'),
(251, 'returns_read', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'orders', 'Xem danh sách đơn trả hàng'),
(253, 'services_read', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'logistic_gateways', 'Xem kết nối HVC'),
(254, 'import', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'orders', 'Nhập file'),
(255, 'export', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'orders', 'Xuất file'),
(256, 'shipments_read', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'orders', 'Xem danh sách đơn giao'),
(257, 'picked_create', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'orders', 'Nhặt hàng'),
(258, 'packed_create', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'orders', 'Đóng gói'),
(259, 'shipping_create', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'orders', 'Xuất kho'),
(260, 'create', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'price_rules', 'Tạo khuyến mại'),
(261, 'read', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'price_rules', 'Xem khuyến mại'),
(262, 'update', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'price_rules', 'Cập nhật khuyến mại'),
(263, 'cancel', 'admin', '2021-06-12 10:11:07', 'admin', 0, 'admin', '2021-06-12 10:11:07', 'admin', 1, 'price_rules', 'Hủy khuyến mại');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=264;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
